var vm = new Vue({
    el: "#app",
    data: {
        arr: ["aa", "bb"]
    },
    methods: {
        add() {
            // 用这样的方式 给数组添加新元素，Vue无法检测到数组的变化，导致页面无法自动更新
            // this.arr[2]="cc"

            Vue.set(this.arr, 2, "cc");
            this.$set(this.arr, 2, "cc")
        }
    }
});