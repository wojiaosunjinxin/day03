var vm = new Vue({
    el: "#app",
    data: {
        listarr: [{ pic: "img1.png", name: "秋刀鱼", price: 35, count: 1 }, { pic: "img1.png", name: "黄焖鸡", price: 20, count: 2 },
        { pic: "img1.png", name: "麻辣小龙虾", price: 120, count: 3 }]
    },
    methods: {
        sub(item) {
            if (item.count > 1) item.count--;
        },
        add(item) {
            if (item.count < 5)
                item.count++;
        },
        clear() {
            this.listarr = [];
        }
    },
    computed: {
        totalprice() {
            var sum = 0;
            this.listarr.forEach(item => {
                sum += item.count * item.price;
            });
            return sum;
        },
        totalcount() {
            var sum = 0;
            this.listarr.forEach(item => {
                sum += item.count;
            });
            return sum;
        }
    }
});