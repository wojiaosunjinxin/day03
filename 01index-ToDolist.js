var vm = new Vue({
    el: "#app",
    data: {
        search: "",
        listArr: []
    },
    methods: {
        addlistArr() {
            if (this.search != "") {
                var obj = { id: this.listArr.length + 1, content: this.search, status: false };
                this.listArr.push(obj);
                this.search = ""
            }
        },
        finish(item) {
            item.status = !item.status;
        },
        deletelist(index, item) {
            this.listArr.splice(index, 1);
            (item + 1).id = this.listArr.length;
            return this.listArr;
        }
    },
    computed: {
        unfinish() {
            var sum = 0;
            let arr = this.listArr.filter((item) => {
                return item.status == false
            });
            return arr.length;
        }
    }
});