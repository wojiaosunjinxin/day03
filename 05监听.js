// Vue实例中的el，data，methods，computed，watch都是Vue实例的选项
// 这些选项的名字是固定的，而且只能写一个
var vm = new Vue({
    el: "#app",
    data: {//定义响应式数据
        inputText: "",
        arr: [],
        obj: { name: "龙龙", age: 30 }
    },
    methods: {//定义方法
        add() {

        }
    },
    computed: {//定义计算属性

    },
    watch: {//设置监听
        inputText(newValue, oldValue) {//每当inputText的值发生改变，inputText这个监听函数就会自动执行，
            // 通过参数--newValue就可以获取inputText的最新值
            //console.log(newValue,this.inputText);
            console.log("获取到了输入的最新值，发起网络请求");
        },
        arr() {//监听数组arr的改变
            console.log("监听到arr发生改变了");
        },
        obj: {//监听对象obj的改变
            handler() {//监听回调
                console.log("监听到obj发生改变了");
            },
            deep: true,//开启深度监听
            immediate: true,//初始化执行监听回调
        }

    }
});